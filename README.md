# Angular 6 + WebAPI task

It is a test task

## Getting Started

The solution contains two parts: API and web application that uses this API.

### Prerequisites

1. .NET Core SDK 2.1.101 or higher (https://www.microsoft.com/net/download/thank-you/dotnet-sdk-2.1.101-windows-x64-installer)
2. MSBuild 15 that can be installed with .NET Core SDK (or with Visual Studio 2107 Community)
3. Path to MsBuild.exe should be located in PATH system variable. By default, it is the following "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin"
4. NPM that can be installed with Node.js (https://nodejs.org)
5. Execute 'npm install' command to install all necessary dependencies
6. Install Angular CLI (npm install -g @angular/cli)

```
npm install
npm install -g @angular/cli
```

## Setting Up API

API port must be 5000 to let dev server use '/api/' paths. If port is other it will be necessary to change port in webpack.config.js

There are two ways to set up API:

1. Start as IIS Express host from Visual Studio Community 2017 (starting by scripts doesn't work for a while)
2. Build by 'npm run api-build' command and then publish by 'npm run api-publish' command. Then it is necessary to deploy IIS host manually and choose 'dist_api' folder as source folder

```
npm run api-build
npm run api-publish
```

## Running Web Application

Execute the next command in console
```
npm start
```

## Fields for improvement
1. Try to adapt Redux
2. Make some components reusable (dropdowns are candidates)