import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import CountryListComponent from "./country-list/country-list.component";
import VisitsReportComponent from "./visits-report/visits-report.component";

const routes: Routes = [
  { path: '', redirectTo: '/countries', pathMatch: 'full' },
  { path: 'countries', component: CountryListComponent },
  { path: 'visits', component: VisitsReportComponent }
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule { }
