import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import CountryListComponent from "./country-list/country-list.component";
import VisitsReportComponent from "./visits-report/visits-report.component";
import TouristDDListComponent from './tourist-dd-list/tourist-dd-list.component';
import YearDdListComponent from './year-dd-list/year-dd-list.component';
import VisitListComponent from './visit-list/visit-list.component';
import VisitsReceiverButtonComponent from './visits-receiver-button/visits-receiver-button.component';

@NgModule({
  declarations: [
    AppComponent,
    CountryListComponent,
    VisitsReportComponent,
    TouristDDListComponent,
    YearDdListComponent,
    VisitListComponent,
    VisitsReceiverButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
