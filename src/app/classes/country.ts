export default class Country {
    id: string;
    name: string;
    description: string;
}