import { Component, OnInit } from "@angular/core";
import Country from "../classes/country";
import TouristicService from "../touristic.service";

@Component({
  selector: "country-list",
  templateUrl: "./country-list.component.html",
  styleUrls: ["./country-list.component.css"]
})
export default class CountryListComponent implements OnInit {
  private countries: Country[];

  constructor(private touristicService: TouristicService) { }

  ngOnInit() {
    this.touristicService.getCountries().subscribe(countries => this.countries = countries);
  }

}
