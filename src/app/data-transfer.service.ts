import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs';

@Injectable()
export default class DataTransferService {
    private selesctedTouristIdSource = new Subject<string>();
    private selesctedYearSource = new Subject<string>();
    private showVisitsMomentSource = new Subject<void>();
    
    selesctedTouristId$ = this.selesctedTouristIdSource.asObservable();
    selesctedYear$ = this.selesctedYearSource.asObservable();
    showVisitsMoment$ = this.showVisitsMomentSource.asObservable();

    selectTourist(touristId: string) {
        this.selesctedTouristIdSource.next(touristId);
    }

    selectYear(year: string) {
        this.selesctedYearSource.next(year);
    }

    showVisits() {
        this.showVisitsMomentSource.next();
    }
}
