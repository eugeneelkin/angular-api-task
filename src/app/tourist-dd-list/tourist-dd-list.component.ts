import { Component, OnInit } from "@angular/core";
import Tourist from "../classes/tourist"
import TouristicService from "../touristic.service";
import DataTransferService from '../data-transfer.service';
import { Subscription } from "rxjs";

@Component({
    selector: 'tourist-dd-list',
    templateUrl: './tourist-dd-list.component.html',
    styleUrls: ['./tourist-dd-list.component.css']
})

export default class TouristDDListComponent implements OnInit {
    private empty: Tourist = { id: "-1", name: "-- none --" };
    private tourists: Tourist[] = [this.empty];
    private subscription: Subscription;

    constructor(private touristicService: TouristicService, private dataTransferService: DataTransferService) {
        this.subscription = this.touristicService.getTourists().subscribe(
            tourists => {
                this.tourists = [this.empty, ...tourists]
            }
        );
    }

    ngOnInit() {
        
    }

    onSelect(touristId: string) {
        this.dataTransferService.selectTourist(touristId);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
