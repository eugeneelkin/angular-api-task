import { TestBed, inject } from '@angular/core/testing';

import TouristicService from './touristic.service';

describe('TouristicService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TouristicService]
    });
  });

  it('should be created', inject([TouristicService], (service: TouristicService) => {
    expect(service).toBeTruthy();
  }));
});
