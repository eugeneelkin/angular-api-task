import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from 'rxjs';
import Country from "./classes/country";
import Tourist from "./classes/tourist";
import Year from "./classes/year";
import Visit from "./classes/visit";

@Injectable({
    providedIn: "root"
})
export default class TouristicService {
    private getCountriesUrl = "/api/countries";
    private getTouristsUrl = "/api/tourists";
    private getTouristVisitsYearsUrl = "/api/tourists/{0}/countries/years";
    private getTouristVisitsUrl = "/api/tourists/{0}/countries?grouped=true&year="

    constructor(private http: HttpClient) { }

    getCountries(): Observable<Country[]> {
        return this.http.get<Country[]>(this.getCountriesUrl);
    }

    getTourists(): Observable<Tourist[]> {
        return this.http.get<Tourist[]>(this.getTouristsUrl)
    }

    getTouristVisitsYears(touristId: string): Observable<Year[]> {
        return this.http.get<Year[]>(this.getTouristVisitsYearsUrl.replace(/\{0\}/g, touristId))
    }

    getTouristVisits(touristId: string, year: string) {
        return this.http.get<Visit[]>(this.getTouristVisitsUrl.replace(/\{0\}/g, touristId) + year);
    }
}
