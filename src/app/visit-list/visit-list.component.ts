import { Component, OnInit } from "@angular/core";
import TouristicService from "../touristic.service";
import DataTransferService from "../data-transfer.service";
import Visit from "../classes/visit";
import { Subscription } from "rxjs";

@Component({
    selector: "visit-list",
    templateUrl: "./visit-list.component.html",
    styleUrls: ["./visit-list.component.css"]
})

export default class VisitListComponent implements OnInit {
    private selectedTourist: string = null;
    private selectedYear: string = null;
    private visits: Visit[];
    private selectedTouristSubscription: Subscription;
    private selectedYearSubscription: Subscription;
    private showVisitsSunscription: Subscription;

    constructor(private touristicService: TouristicService, private dataTransferService: DataTransferService) {
        this.selectedTouristSubscription = this.dataTransferService.selesctedTouristId$.subscribe(
            touristId => {
                this.visits = null;
                if (touristId === "-1") {
                    this.selectedTourist = null;
                } else {
                    this.selectedTourist = touristId;
                }
            }
        );

        this.selectedYearSubscription = this.dataTransferService.selesctedYear$.subscribe(
            year => {
                this.visits = null;
                if (year === "-- none --") {
                    this.selectedYear = null;
                } else {
                    this.selectedYear = year;
                }
            }
        );

        this.showVisitsSunscription = this.dataTransferService.showVisitsMoment$.subscribe(
            () => {
                if (this.selectedTourist && this.selectedYear) {
                    this.touristicService.getTouristVisits(this.selectedTourist, this.selectedYear).subscribe(
                        visits => this.visits = visits
                    );
                }
            }
        );
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.selectedTouristSubscription.unsubscribe();
        this.selectedYearSubscription.unsubscribe();
        this.showVisitsSunscription.unsubscribe();
    }
}
