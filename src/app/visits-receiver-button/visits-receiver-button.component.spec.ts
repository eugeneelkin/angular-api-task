import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import VisitsReceiverButtonComponent from './visits-receiver-button.component';

describe('VisitsReceiverButtonComponent', () => {
  let component: VisitsReceiverButtonComponent;
  let fixture: ComponentFixture<VisitsReceiverButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitsReceiverButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitsReceiverButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
