import { Component, OnInit } from '@angular/core';
import DataTransferService from '../data-transfer.service';
import { Subscription } from "rxjs";

@Component({
    selector: 'visits-receiver-button',
    templateUrl: './visits-receiver-button.component.html',
    styleUrls: ['./visits-receiver-button.component.css']
})
export default class VisitsReceiverButtonComponent implements OnInit {
    constructor(private dataTransferService: DataTransferService) { }

    ngOnInit() {
    }

    onClick() {
        this.dataTransferService.showVisits();
    }

}
