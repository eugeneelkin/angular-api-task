import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import VisitsReportComponent from './visits-report.component';

describe('VisitsReportComponent', () => {
  let component: VisitsReportComponent;
  let fixture: ComponentFixture<VisitsReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitsReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitsReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
