import { Component, OnInit } from '@angular/core';
import DataTransferService from '../data-transfer.service';

@Component({
    selector: 'visits-report',
    templateUrl: './visits-report.component.html',
    styleUrls: ['./visits-report.component.css'],
    providers: [DataTransferService]
})
export default class VisitsReportComponent implements OnInit {

    constructor(private dataTransferService: DataTransferService) { }

    ngOnInit() { }
}
