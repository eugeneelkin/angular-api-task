import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import YearDdListComponent from './year-dd-list.component';

describe('YearDdListComponent', () => {
  let component: YearDdListComponent;
  let fixture: ComponentFixture<YearDdListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YearDdListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YearDdListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
