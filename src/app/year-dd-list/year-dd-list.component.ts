import { Component, OnInit } from "@angular/core";
import TouristicService from "../touristic.service";
import DataTransferService from "../data-transfer.service";
import Year from "../classes/year";
import { Subscription } from "rxjs";

@Component({
    selector: 'year-dd-list',
    templateUrl: './year-dd-list.component.html',
    styleUrls: ['./year-dd-list.component.css']
})

export default class YearDdListComponent implements OnInit {
    private empty: Year = { name: "-- none --" };
    private years: Year[] = [this.empty];
    private subscription: Subscription;

    constructor(private touristicService: TouristicService, private dataTransferService: DataTransferService) {
        this.subscription = this.dataTransferService.selesctedTouristId$.subscribe(
            touristId => {
                if (touristId === "-1") {
                    this.years = [this.empty];
                    this.dataTransferService.selectYear("-- none --");
                } else {
                    this.touristicService.getTouristVisitsYears(touristId).subscribe(years => this.years = [this.empty, ...years]);
                }
            }
        );
    }

    ngOnInit() {
    }

    onSelect(year: string) {
        this.dataTransferService.selectYear(year);
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
