﻿namespace BaseEntities.Entities
{
    public interface IVersionedEntity: IEmptyEntity
    {
        byte[] RowVersion { get; set; }
    }
}
