﻿namespace DataWorkShop.Entities
{
    using BaseEntities.Entities;

    public class Country: BaseEntity<string>
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
