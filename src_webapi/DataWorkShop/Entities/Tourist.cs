﻿namespace DataWorkShop.Entities
{
    using BaseEntities.Entities;

    public class Tourist : BaseEntity<string>
    {
        public string Name { get; set; }
    }
}
