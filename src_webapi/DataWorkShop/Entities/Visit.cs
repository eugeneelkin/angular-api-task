﻿namespace DataWorkShop.Entities
{
    using System.ComponentModel.DataAnnotations.Schema;
    using BaseEntities.Entities;
    using System;

    public class Visit : BaseEntity<string>
    {
        public DateTime DateOfVisit { get; set; }

        public string CountryId { get; set; }
        [ForeignKey("CountryId")]
        public virtual Country Country { get; set; }

        public string TouristId { get; set; }
        [ForeignKey("TouristId")]
        public virtual Tourist Tourist { get; set; }

    }
}
