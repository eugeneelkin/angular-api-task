﻿namespace DataWorkShop.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using DataWorkShop.Entities;
    using Microsoft.EntityFrameworkCore;
    using System;

    public static class ExtensionsForTouristicDBContext
    {
        public static void EnsureSeedData(this TouristicDBContext context)
        {
            // Apply seeding only if tables are empty.
            // Also, we check that there isn't any migration because seeding can be triggered during "Add-Migration InitialCreate" command.
            if (context.Database.GetAppliedMigrations().Any()
                && !context.Database.GetPendingMigrations().Any()
                && !context.Countries.Any()
                && !context.Tourists.Any()
                && !context.Visits.Any())
            {
                context.Countries.Add(new Country {
                    Name = "Italy",
                    Description = "Somewhere in Eutope"
                });
                context.Countries.Add(new Country
                {
                    Name = "Germany",
                    Description = "Beer and sausages"
                });
                context.Countries.Add(new Country
                {
                    Name = "Grece",
                    Description = "Many islands, Olympic Games inventor, delicious dishes"
                });
                context.Countries.Add(new Country
                {
                    Name = "Netherlands",
                    Description = "Bicycles, tulips"
                });
                context.Countries.Add(new Country
                {
                    Name = "India",
                    Description = "Cheap and unpredictable"
                });
                context.Countries.Add(new Country
                {
                    Name = "Russia",
                    Description = "Try to survive"
                });
                context.Countries.Add(new Country
                {
                    Name = "Turkey",
                    Description = @"If you hear ""Tagil!!!"" it will be better just to run away somewhere"
                });
                context.Countries.Add(new Country
                {
                    Name = "Australia",
                    Description = "Far and beatiful"
                });
                context.Countries.Add(new Country
                {
                    Name = "Brazil",
                    Description = "Beautiful and energetic people, especially women"
                });
                context.Countries.Add(new Country
                {
                    Name = "USA",
                    Description = "All significant things happen there"
                });

                for (var i = 0; i < 100; i++)
                {
                    context.Tourists.Add(new Tourist
                    {
                        Name = $"Tourist {i}"
                    });
                }

                context.SaveChanges();

                var touristList = context.Tourists.ToList();
                var countryList = context.Countries.ToList();
                Random rnd = new Random();

                for (var i = 0; i < 1000; i++)
                {
                    context.Visits.Add(new Visit
                    {
                        DateOfVisit = ExtensionsForTouristicDBContext.RandomDay(rnd),
                        TouristId = touristList[rnd.Next(0, 99)].Id,
                        CountryId = countryList[rnd.Next(0, 9)].Id
                    });
                }

                context.SaveChanges();
            }
        }

        private static DateTime RandomDay(Random rnd)
        {
            DateTime start = new DateTime(1995, 1, 1);
            int range = (DateTime.Today - start).Days;
            return start.AddDays(rnd.Next(range));
        }
    }
}
