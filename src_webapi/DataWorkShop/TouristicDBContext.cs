﻿namespace DataWorkShop
{
    using DataWorkShop.Entities;
    using Microsoft.EntityFrameworkCore;

    public class TouristicDBContext : DbContext
    {
        public TouristicDBContext() : base()
        {

        }

        public TouristicDBContext(DbContextOptions options): base(options)
        {

        }

        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Tourist> Tourists { get; set; }
        public virtual DbSet<Visit> Visits { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Country>()
            .HasIndex(u => u.Name)
            .IsUnique();
        }
    }
}
