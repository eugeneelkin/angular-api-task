﻿namespace Touristization.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AutoMapper;
    using DataInstructions.Instructions;
    using DataInstructions.Instructions.Structures;
    using DataWorkShop;
    using DataWorkShop.Entities;
    using Microsoft.AspNetCore.Mvc;
    using TouristicApi.Models;
    

    [Produces("application/json")]
    [Route("api/Countries")]
    public class CountriesController : Controller
    {
        private readonly TouristicDBContext context;

        public CountriesController(TouristicDBContext context)
        {
            this.context = context;
        }

        // GET api/countries
        [HttpGet]
        public async Task<IActionResult> GetCountries(string orderByField = null, bool isDescending = false, int? pageSize = null, int? pageAt = null)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var countries = await new ReceivingListInstruction<Country, string>(this.context,
                new ListInstructionParams<Country>
                {
                    orderByField = orderByField,
                    isDescending = isDescending,
                    pageAt = pageAt,
                    pageSize = pageSize
                }).Execute();

            var sanitizedCountries = Mapper.Map<IEnumerable<Country>, IEnumerable<CountryViewModel>>(countries);

            return Ok(sanitizedCountries);
        }
    }
}
