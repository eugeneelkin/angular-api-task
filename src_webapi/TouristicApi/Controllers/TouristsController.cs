namespace TouristicApi.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AutoMapper;
    using DataInstructions.Instructions;
    using DataInstructions.Instructions.Structures;
    using DataWorkShop;
    using DataWorkShop.Entities;
    using Microsoft.AspNetCore.Mvc;
    using TouristicApi.Models;

    [Produces("application/json")]
    [Route("api/Tourists")]
    public class TouristsController : Controller
    {
        private readonly TouristicDBContext context;

        public TouristsController(TouristicDBContext context)
        {
            this.context = context;
        }

        // GET api/tourists
        [HttpGet]
        public async Task<IActionResult> GetTourists(string orderByField = null, bool isDescending = false, int? pageSize = null, int? pageAt = null)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tourists = await new ReceivingListInstruction<Tourist, string>(this.context,
                new ListInstructionParams<Tourist>
                {
                    orderByField = orderByField,
                    isDescending = isDescending,
                    pageAt = pageAt,
                    pageSize = pageSize
                }).Execute();

            var sanitizedTourists = Mapper.Map<IEnumerable<Tourist>, IEnumerable<TouristViewModel>>(tourists);

            return Ok(sanitizedTourists);
        }

        [HttpGet("{id}/Countries")]
        public async Task<IActionResult> GetTouristCountries(string id, string orderByField = null, bool isDescending = false, int? pageSize = null, int? pageAt = null, bool grouped = false, int? year = null)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var instructionParams = new GroupedListInstructionParams<Visit>
            {
                orderByField = orderByField,
                isDescending = isDescending,
                pageAt = pageAt,
                pageSize = pageSize,
                filterExpr = visit => visit.TouristId == id,
                navigationProperties = new string[] { "Country" },
                groupExpr = visit => visit.Country.Name
            };

            if (year.HasValue && year > 1000 && year < 9999)
            {
                instructionParams.filterExpr = visit => visit.TouristId == id && visit.DateOfVisit.Year == year;
            }

            if (grouped)
            {
                var groupedVisits = await new ReceivingGroupedListInstruction<Visit, string>(this.context, instructionParams).Execute();
                var sanitizedGroupedVisits = Mapper.Map<IEnumerable<GroupedItem>, IEnumerable<TouristCountriesViewModel>>(groupedVisits);
                return Ok(sanitizedGroupedVisits);
            }

            var visits = await new ReceivingListInstruction<Visit, string>(this.context, instructionParams).Execute();
            var sanitizedVisits = Mapper.Map<IEnumerable<Visit>, IEnumerable<VisitViewModel>>(visits);
            return Ok(sanitizedVisits);
        }

        [HttpGet("{id}/Countries/Years")]
        public async Task<IActionResult> GetTouristCountriesYears(string id, string orderByField = null, bool isDescending = false, int? pageSize = null, int? pageAt = null)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var instructionParams = new GroupedListInstructionParams <Visit>
            {
                orderByField = orderByField,
                isDescending = isDescending,
                pageAt = pageAt,
                pageSize = pageSize,
                filterExpr = visit => visit.TouristId == id,
                groupExpr = visit => visit.DateOfVisit.Year.ToString()
            };

            var groupedVisits = await new ReceivingGroupedListInstruction<Visit, string>(this.context, instructionParams).Execute();
            var sanitizedGroupedVisits = Mapper.Map<IEnumerable<GroupedItem>, IEnumerable<TouristCountriesViewModel>>(groupedVisits);
            return Ok(sanitizedGroupedVisits);
        }
    }
}
