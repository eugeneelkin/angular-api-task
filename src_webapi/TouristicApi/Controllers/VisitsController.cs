﻿namespace TouristicApi.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using AutoMapper;
    using DataInstructions.Instructions;
    using DataInstructions.Instructions.Structures;
    using DataWorkShop;
    using DataWorkShop.Entities;
    using Microsoft.AspNetCore.Mvc;
    using TouristicApi.Models;    

    [Produces("application/json")]
    [Route("api/Visits")]
    public class VisitsController : Controller
    {
        private readonly TouristicDBContext context;

        public VisitsController(TouristicDBContext context)
        {
            this.context = context;
        }

        // GET api/visits
        [HttpGet]
        public async Task<IActionResult> GetVisits(string orderByField = null, bool isDescending = false, int? pageSize = null, int? pageAt = null)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var visits = await new ReceivingListInstruction<Visit, string>(this.context,
                new ListInstructionParams<Visit>
                {
                    orderByField = orderByField,
                    isDescending = isDescending,
                    pageAt = pageAt,
                    pageSize = pageSize,
                    navigationProperties = new string[] { "Tourist", "Country" }
                }).Execute();

            var sanitizedVisits = Mapper.Map<IEnumerable<Visit>, IEnumerable<VisitViewModel>>(visits);

            return Ok(sanitizedVisits);
        }
    }
}