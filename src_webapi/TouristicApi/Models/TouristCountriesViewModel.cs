﻿namespace TouristicApi.Models
{
    public class TouristCountriesViewModel
    {
        public string Name { get; set; }
        public int VisitsNumber { get; set; }
    }
}
