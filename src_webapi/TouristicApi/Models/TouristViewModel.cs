﻿namespace TouristicApi.Models
{
    public class TouristViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
