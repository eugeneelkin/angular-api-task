﻿namespace TouristicApi.Models
{
    using System;

    public class VisitViewModel
    {
        public string Id { get; set; }
        public DateTime DateOfVisit { get; set; }
        public TouristViewModel Tourist { get; set; }
        public CountryViewModel Country { get; set; }
    }
}
