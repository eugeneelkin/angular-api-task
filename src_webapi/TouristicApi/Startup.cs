﻿namespace Touristization
{
    using AutoMapper;
    using DataInstructions.Instructions.Structures;
    using DataWorkShop;
    using DataWorkShop.Entities;
    using DataWorkShop.Extensions;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using TouristicApi.Models;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<TouristicDBContext>(options => options.UseSqlServer(Configuration.GetConnectionString("TouristicDatabase"), b => b.MigrationsAssembly("DataWorkShop")));
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                using (var serviceScope = app.ApplicationServices.CreateScope())
                {
                    var context = serviceScope.ServiceProvider.GetService<TouristicDBContext>();

                    // Seed the database.
                    context.Database.Migrate();
                    context.EnsureSeedData();
                }
            }

            app.UseMvc();

            Mapper.Initialize(mapper => {
                mapper.CreateMap<Country, CountryViewModel>()
                .ForSourceMember(sm => sm.RowVersion, opt => opt.Ignore());

                mapper.CreateMap<Tourist, TouristViewModel>()
                .ForSourceMember(sm => sm.RowVersion, opt => opt.Ignore());

                mapper.CreateMap<Visit, VisitViewModel>()
                .ForSourceMember(sm => sm.RowVersion, opt => opt.Ignore());

                mapper.CreateMap<GroupedItem, TouristCountriesViewModel>()
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Key))
                .ForMember(dest => dest.VisitsNumber, opt => opt.MapFrom(src => src.AggreagtionResult));
            });
        }
    }
}
